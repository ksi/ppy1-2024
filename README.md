# PPY1 2024

## ⚠️⚠️⚠️ In case your time table does not allow you to attend the lectures ⚠️⚠️⚠️

We are recording the lectures on Teams, and you will have access to the recordings, if this is your situation. Please let me know why on matej.mojzes@fjfi.cvut.cz, and I will grant you access.

Otherwise, it is mandatory (and much more fun!) to attend the lecture live. Thanks :)


## In this repository you can find:

* whatever we are looking at during lectures, see the `lecture_<lecture_number>_<topic>.ipynb` Jupyter notebooks, especially in the first one you can find the criteria for "zápočet" (tl;dr it is an individual project which needs to cover at least three topics covered during lectures)
* student projects will be added to the `projects/` folder over time (via merge requests)


## Project submission (Merge Request, or just MR) cookbook

Note: as some students pointed out, you can use the [built-in GitLab web editor(s)](https://docs.gitlab.com/ee/user/project/repository/web_editor.html) to work on your project and MR. We will try to add more information on this option over time.

### Prerequisites:
1. Make sure you have [Git installed](https://git-scm.com/downloads) on your local machine.
2. You need to have access to your [GitLab account](https://gitlab.fjfi.cvut.cz)
3. Optional, but highly reccommended: SSH key on your machine and in GitLab — here you can find instructions how to set it up https://docs.gitlab.com/ee/user/ssh.html (go for and RSA variant, and basically go with the defaults, unless you know exactly what you are doing...)
   - If you decide to skip using SSH, you can proceed using the HTTPS authentication — it is less convenient, but will do the trick anyways

### Steps:


#### 1. Fork the Repository:
   1. Go to the repository you want to contribute to in GitLab: https://gitlab.fjfi.cvut.cz/ksi/ppy1-2024.
   2. Click on the "Fork" button.
   3. Choose your account or group where you want to fork the repository.
   4. GitLab will create a copy of the repository under your account or group.


#### 2. Clone Your Fork:
   Clone your forked repository to your local machine:
   ```
   git clone <your_fork_url>
   ```
   Replace `<your_fork_url>` with the URL of your forked repository.
   

#### 3. Create a New Branch:
   Create a new branch for your changes based on the main branch of your fork:
   ```
   git checkout -b <branch_name> origin/main
   ```
   Replace `<branch_name>` with the name of your new branch.


#### 4. Make Changes:
   Make the necessary changes — ie. place all your files under the `projects/<your_user_name>/` folder (this means you should create your own subfolder in the `projects` folder). Make sure only the relevant files will be included.
   
   You can run `git status` to see the list of files to be added.


#### 5. Stage and Commit Changes:
   Stage the changes you want to include in the merge request:
   ```
   git add .
   ```
   Again, you can run `git status` to check what will be added and modify if needed. 
   
   Then, you can commit the staged changes:
   ```
   git commit -m "Your commit message here"
   ```
   Replace `"Your commit message here"` with a brief description of the changes you made.


#### 6. Push Changes to Your Fork:
   Push your branch to your forked repository on GitLab:
   ```
   git push origin <branch_name>
   ```

#### 7. Create a Merge Request:
   1. Go to your forked repository on GitLab in your web browser.
   2. Switch to the branch you just pushed using the branch dropdown menu.
   3. Click on the "Merge Request" button.
   4. Fill out the necessary details for the merge request, including:
      - Title: A descriptive title for your merge request, ie. project.
      - Description: If it is not clear, you can help navigate through your solution.
      - ~~Assignee & Reviewer: Select Matej Mojzeš / @mojzemat~~. (Apparently, this is not supported).
   5. Click on the "Submit merge request" button.

#### 8. Review and Merge:
   Once the merge request is submitted, it will be reviewed by your lecturer. During the review process you might need to add and push one or more commits reflecting the review & discussion around it. It helps to address each individual point (or comment) in an individual commit. Once approved, your work will be added to the main branch of the original repository.

## License

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
