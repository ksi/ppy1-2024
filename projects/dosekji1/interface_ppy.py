import interface_1f_ppy
import interface_2f_ppy
from pathlib import Path
import datetime
import json


def interface_1():
    while True:
        print("menu:")
        print("1. Load diary")
        print("2. Make new diary")
        print("3. Remove diary")
        print("4. Quit")

        try:
            choice = int(input("Enter your choice(number): ").strip())
        except ValueError:
            print("Invalid input. Please enter a number you are able to see on the screen.")
            continue

        if choice == 1:
            interface_1f_ppy.load()
        elif choice == 2:
            interface_1f_ppy.make_new()
        elif choice == 3:
            interface_1f_ppy.remove()
        elif choice == 4:
            print("bye bye")
            break
        else:
            print("Chose numbers from numbers you are able to see")


def interface_2(filename):
    while True:
        print("Reader's diary menu:")
        print("1. Add book")
        print("2. Delete book based on its Title or Author")
        print("3. Search books based on its Title or Author")
        print("4. Change attribues")
        print("5. Delete all records of all books")
        print("6. Quit")

        try:
            second_choice = int(input("Enter your choice: ").strip())
        except ValueError:
            print("Invalid input. Please enter a number.")
            continue

        if second_choice == 1:
            interface_2f_ppy.choice_1(filename)
        elif second_choice == 2:
            interface_2f_ppy.choice_2(filename)
        elif second_choice == 3:
            interface_2f_ppy.choice_3(filename)
        elif second_choice == 4:
            interface_2f_ppy.choice_4(filename)
        elif second_choice == 5:
            interface_2f_ppy.choice_5(filename)
        elif second_choice == 6:
            break
        else:
            print("Invalid number. Enter number between 1 and 6")