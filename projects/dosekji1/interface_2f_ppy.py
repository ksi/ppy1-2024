from pathlib import Path
import datetime
import json


class NekorektniIndexChyba(Exception):
    pass


class Book:
    def __init__(self, author, title, genres, year, pages, date_read):
        self.author = author
        self.title = title
        self.genres = genres
        self.year = year
        self.pages = pages
        self.date_read = date_read

    def to_dict(self):
        return {
            'author': self.author,
            'title': self.title,
            'genres': self.genres,
            'year': self.year,
            'pages': self.pages,
            'date_read': str(self.date_read) if isinstance(self.date_read, datetime.datetime) else self.date_read
        }


def load_books(filename):
    try:
        with open(filename + ".txt", "r") as file:
            lines = file.readlines()  # otevře soubor a dá ho do seznamu
    except FileNotFoundError:
        print("File not found, make sure the file exists")  # pokud existuje
        return []

    books = []
    for i, line in enumerate(lines):  # bez i a enumerate --> nasot
        try:
            book_dict = json.loads(line)  # do seznamu books nahraje jednotlivé slovníky a vrátí je
            book = Book(**book_dict)
            books.append(book)
        except json.JSONDecodeError:
            print(f"Error decoding JSON at line {i + 1}. Skipping this line.")  # chyba při dekódování dat
            continue

    return books


def save_books(filename, books):
    with open(filename + ".txt", "w") as file:  # otevře znovu soubor a přepíše knihy podle seznamu slovníků
        for book in books:
            json.dump(book.to_dict(), file)  # file.write(json.dumps(book.to_dict()) + '\n')
            file.write('\n')


def choice_1(filename):
    try:  # Přidá knihu
        author = input("Enter the author: ").strip()
        title = input("Enter the title: ").strip()
        genres = input("Enter the genres (comma-separated): ")
        year = int(input("Enter the year of publication: ").strip())
        pages = int(input("Enter the number of pages: ").strip())
    except ValueError:
        print("Invalid input. Please enter data in right format.")
        return
    genres_list = [genre.strip() for genre in genres.split(',')]  # ostripuje jednotlivé položky v seznamu

    date_read = datetime.datetime.strptime(input("Enter the date read (YYYY-MM-DD): ").strip(),
                                           "%Y-%m-%d")  # metoda na zápis přesného času až na microsekundy omezená na roky měsíce a dny
    book = Book(author, title, genres_list, year, pages, date_read)  # použití class book
    with open(filename + ".txt", "a") as file:
        json.dump(book.to_dict(),
                  file)  # metoda json.dump s pomocí classBook def to_dict() zapíše slovník do souboru s koncovkou .txt
        file.write('\n')  # ale data jsou pořád jako .json formát jsou ale čitelná když otevřu .txt dokument


def choice_2(filename):  # smazaní knihy podle autorů a titulů
    search_key = input("Enter the author or title to find the book: ").strip()

    found_books = []
    books = load_books(filename)  # vrátí seznam slovníků

    displayed_index = 1

    for i, book in enumerate(books, start=1):
        if search_key.lower() in [book.author.lower(), book.title.lower()]:  # najde podle autora a titulu
            found_books.append(book)  # když nějaké najde přidá je do found_books
            print(f"{displayed_index}. Author: {book.author}, Title: {book.title}")
            displayed_index += 1
            # vypíše všechny autory a tituly

    if not found_books:
        print("No matching books found.")
    else:
        try:
            book_index = int(input("Enter the number of the book to delete: ").strip()) - 1
            if 0 <= book_index < len(found_books):
                deleted_book = found_books.pop(book_index)  # Smazání knihy z nalezených knih
                print(f"Deleted book: {deleted_book.title} by {deleted_book.author}")
                books.remove(deleted_book)
                save_books(filename, books)  # Uložení zbývajících knih zpět do souboru
            else:
                print("Invalid book index.")
        except ValueError:
            print("Invalid input. Please enter a valid number.")


def choice_3(filename):  # vypíše knihu podle jmena autorů a titulů
    try:
        with open(filename + ".txt", "r") as file:
            lines = file.readlines()  # vytvoří seznam slovníků
    except FileNotFoundError:
        print("File not found, make sure the file exists")
        return

    search_key = input("Enter the author or title to find the book: ").strip()

    found_books = []
    for i, line in enumerate(lines):  # projede seznam slovníků loopem
        try:
            book_dict = json.loads(line)  # načte se slovník do book_dict
            book = Book(
                **book_dict)  # tady se vytvoří instance třídy Book ze slovníku a přijme parametry ze slovníky podle class book
        except json.JSONDecodeError:  # pro chyb při dekódování dat
            print(f"Error decoding JSON at line {i + 1}. Skipping this line.")
            continue

        if search_key.lower() in [book.author.lower(), book.title.lower()]:
            found_books.append(book)  # najde a přidá podle title nebo author do prázdného seznamu
            # takže mám zase seznam slovníků
    if not found_books:
        print("No matching books found.")
    else:
        for i, book in enumerate(found_books, start=1):  # o indexuju je a vypíšu je
            book_info = book.to_dict()
            print(
                f"{i}. Author: {book_info['author']}, Title: {book_info['title']}, Genres: {book_info['genres']}, Year: {book_info['year']}, Pages: {book_info['pages']}, Date Read: {book_info['date_read']}")


def choice_4(filename):  # změní údaje o knize
    search_key = input("Enter the author or title to find the book: ").strip()

    try:

        with open(filename + ".txt", "r") as file:
            lines = file.readlines()  # udělá seznam ze všech slovníků
    except FileNotFoundError:  # vyjímka ošetří chybu kdyby nešlo najít soubor
        print("File not found, make sure the filr exists")
        return
    found_books = []
    book_positions = []
    all_books = []
    for i, line in enumerate(
            lines):  # Enumerate --> slouží k vytvoření objektu, který kombinuje iterovatelný objekt (například seznam, tuple nebo řetězec) a generuje dvojice index-hodnota během iterace
        try:  # enumerate(iterable, start=0) iterable: Iterovatelný objekt, pro který chcete vytvořit enumeraci (seznam, tuple, řetězec) a start: Volitelný parametr, který určuje počáteční hodnotu indexu. Výchozí hodnota je 0
            book_dict = json.loads(line)
        except json.JSONDecodeError:  # je vyvolaná při chybě dekódování dat(chybný formát JSON, nečekaný datový typ, jiné problémy s dekódováním)
            print(f"Error decoding JSON at line {i + 1}. Skipping this line.")
            continue
        all_books.append(book_dict)
        if search_key.lower() in [book_dict.get('author', '').lower(), book_dict.get('title',
                                                                                     '').lower()]:  # funkce .get preventuje proti KeyError a buď najde hodnotu klíče, nebo vrátí prázdný řetězec
            found_books.append(book_dict)
            book_positions.append(i)

        # načte na var vždy jeden slovník a porovná key s inputem uživatele a pokud je shodný pak ho přidá do seznamu
        # kvůli přepisování nezměněných knih

    if not found_books:
        print("No matching books found.")  # kontrala existenece knihy
    else:
        for i, book in enumerate(found_books,
                                 start=1):  # pro případ kdyby našel krok předtím více knih podle jména autora a vypíše všechny knihy co našel
            print(
                f"{i}. Author: {book.get('author', 'Unknown')}, Title: {book.get('title', 'Unknown')}")  # Pokud klíč neexistuje, použije se výchozí hodnota 'Unknown'

        try:  # vyjímka když uživatel nezadá platný index knihy
            book_index = int(input("Enter the number of the book to edit: ").strip()) - 1
            if book_index < 0 or book_index >= len(
                    found_books):  # když není vybrané číslo v rozmezí intervalu vypsaných knih
                raise NekorektniIndexChyba("Invalid book index.")

            selected_book = found_books[book_index]
            all_i = all_books.index(selected_book)

            # Tady je kod přímo na upravu knihy
            print("Selected book:", selected_book)
            print("1. author")
            print("2. title")
            print("3. genres")
            print("4. year")
            print("5. pages")
            print("6. date_read")
            try:
                change_input = int(input("select which atribute you want to change: ").strip())
                if change_input == 1:
                    author_change = input("Change author: ").strip()
                    selected_book['author'] = author_change
                elif change_input == 2:
                    title_change = input("Change title: ").strip()
                    selected_book['title'] = title_change
                elif change_input == 3:
                    genres_change = input("Change genres: ").split(',')
                    selected_book['genres'] = genres_change
                elif change_input == 4:
                    year_change = int(input("Change year: ").strip())
                    selected_book['year'] = year_change
                elif change_input == 5:
                    pages_change = int(input("Change pages: ").strip())
                    selected_book['pages'] = pages_change
                elif change_input == 6:
                    date_change = datetime.datetime.strptime(input("Change date (YYYY-MM-DD): ").strip(), "%Y-%m-%d")
                    selected_book['date_read'] = date_change
            except ValueError:
                print("Invalid input. Please enter a number you are able to see on the screen.")
                pass
            # konec části kde se upravují jednotlivé položky
            all_books[all_i] = selected_book

            updated_lines = [json.dumps(book) + '\n' for book in all_books]

            with open(filename + ".txt", "w") as file:
                file.writelines(updated_lines)

            print("Book updated successfully.")

        except (ValueError, NekorektniIndexChyba, IndexError) as e:
            print(f"Invalid input: {e}. Please enter a valid number.")
        except Exception as e:
            print(f"An unexpected error occurred: {e}")


def choice_5(filename):  # smaže všechny záznamy
    confirmation = input("Are you sure you want to delete all records? (yes/no): ").lower().strip()

    if confirmation == "yes":
        try:
            with open(filename + ".txt", "w") as file:
                print("All records have been deleted.")  # soubor se přepíše tímto řetězcem (pass)
        except FileNotFoundError:
            print("File not found. Make sure the file exists.")
    else:
        print("No records were deleted.")


def show_all(filename):
    try:
        with open(filename + ".txt", "r") as file:
            lines = file.readlines()
    except FileNotFoundError:
        print("File not found, make sure the file exists")
        return

    found_books = []
    for i, line in enumerate(lines):
        try:
            book_dict = json.loads(line)
            book = Book(**book_dict)
        except json.JSONDecodeError:
            print(f"Error decoding JSON at line {i + 1}. Skipping this line.")
            continue
        found_books.append(book)

    for i, book in enumerate(found_books, start=1):
        book_info = book.to_dict()
        print(
            f"{i}. Author: {book_info['author']}, Title: {book_info['title']}, Genres: {book_info['genres']}, Year: {book_info['year']}, Pages: {book_info['pages']}, Date Read: {book_info['date_read']}")


def sort_pages(filename):
    books = load_books(filename)
    sorted_books = sorted(books, key=lambda x: x.pages, reverse=True)
    save_books(filename, sorted_books)