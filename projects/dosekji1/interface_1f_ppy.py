from pathlib import Path
import interface_ppy
import json


def load():
    while True:
        try:
            filename = input("Enter name of diary without sufix: ").strip()

            if Path(filename + ".txt").exists():  # zkontroluje jestli soubor existuje
                interface_ppy.interface_2(filename)
                break
            else:
                print("File does not exist.")

                try:
                    quit_input = int(input("If you want go back pres 1. If you want try again press 2.").strip())
                except ValueError:
                    print("You must choose a number.")

                if quit_input == 1:
                    break
                elif quit_input == 2:
                    continue
                else:
                    print("You must choose number 1 or 2.")
                continue
        except ValueError:
            print("Invalid input try again.")
            continue


def make_new():
    try:
        filename = input("Enter the name of the new diary without suffix: ").strip()

        if Path(filename + ".txt").exists():  # zkontroluje jestli soubor už existuje
            print("File with this name already exist. Choose diffferent name.")
            return

        with open(filename + ".txt", "w") as file:  # vytvoří nový soubor
            print("New file was made:", filename + ".txt")

        interface_ppy.interface_2(filename)


    except ValueError:
        print("Invalid input.")


def remove():
    try:
        file_to_remove = input(
            "Enter the name of the diary to remove (without suffix): ").strip()  # zkontroluje jestli soubor už existuje

        file_path = Path(file_to_remove + ".txt")
        if file_path.exists():

            confirmation = input(f"Are you sure you want to remove {file_path}? (yes/no): ").lower().strip()

            if confirmation == 'yes':

                file_path.unlink()  # Odstraňí soubor
                print(f"File {file_path} removed successfully.")
            else:
                print("Removal canceled.")
        else:
            print("The specified file does not exist.")

    except ValueError:
        print("Invalid input.")
        return