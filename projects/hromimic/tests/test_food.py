import unittest
from food import FoodFactory, Food
from snake import Snake

class TestFood(unittest.TestCase):
    
    def setUp(self) -> None:
        """
        Příprava prostředí pro testy. Vytváří nové instance továrny na jídlo a hada.
        """
        self.food_factory = FoodFactory()
        self.snake = Snake()

    def test_food_spawn_interval(self) -> None:
        """
        Testuje, zda se jídlo vytváří po uplynutí nastaveného intervalu.

        Scénář:
            1. Upravíme čas posledního vytvoření jídla tak, aby byl menší než základní interval.
            2. Spustíme aktualizaci továrny na jídlo.
            3. Zkontrolujeme, zda se nové jídlo vytvořilo.

        Očekávaný výsledek:
            Po uplynutí intervalu by mělo být vytvořeno nové jídlo.
        """
        # Upravíme last_spawn_time, aby se jídlo vytvořilo
        self.food_factory.last_spawn_time -= self.food_factory.base_interval + 1
        self.food_factory.update()
        self.assertGreater(len(self.food_factory.food_list), 0, "Jídlo by mělo být vytvořeno po uplynutí intervalu")

    def test_snake_eats_food(self) -> None:
        """
        Testuje, zda se jídlo správně odstraní, když had jí.

        Scénář:
            1. Vytvoříme jídlo a umístíme ho na pozici hada.
            2. Simulujeme had, který se nachází na místě jídla.
            3. Odstraníme jídlo z továrny na jídlo.
            4. Zkontrolujeme, zda je jídlo odstraněno.

        Očekávaný výsledek:
            Jídlo by mělo být odstraněno ze seznamu po konzumaci hadem.
        """
        # Simulace stavu, kdy had "jí" jídlo
        food = Food()
        food.position = (10, 10)  # Nastavíme pozici jídla
        self.food_factory.food_list = [food]

        # Simulace hadu, který "jí" jídlo
        self.snake.positions = [(10, 10)]  # Had se nachází na místě jídla
        
        # Simulace jídla, které se odstraní, když had jí
        self.food_factory.remove_food(food)
        
        self.assertEqual(len(self.food_factory.food_list), 0, "Jídlo by mělo být odstraněno po konzumaci hadem")

if __name__ == '__main__':
    unittest.main()
