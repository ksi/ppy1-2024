import unittest
from snake import Snake
from food import Food, FoodFactory
from settings import GRID_WIDTH, GRID_HEIGHT

class TestIntegration(unittest.TestCase):
    
    def setUp(self) -> None:
        """
        Příprava prostředí pro testy. Vytváří nové objekty hada a továrny na jídlo před každým testem.
        """
        
        self.snake = Snake()
        self.food_factory = FoodFactory()

    def test_snake_eats_food_and_grows(self) -> None:
        """
        Testuje, zda had správně konzumuje jídlo a zda se jídlo odstraní z továrny na jídlo.

        Scénář:
            1. Umístíme jídlo na pozici hada.
            2. Nastavíme pozici hada na stejnou pozici jako jídlo.
            3. Zkontrolujeme počet kusů jídla před a po odstranění.

        Očekávaný výsledek:
            1. Jídlo by mělo být přítomné v továrně na jídlo před odstraněním.
            2. Jídlo by mělo být odstraněno po konzumaci hadem.
        """

        food = Food()
        food.position = (GRID_WIDTH // 2, GRID_HEIGHT // 2)  # Jídlo je na pozici hada
        self.food_factory.food_list = [food]
        
        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]  # Had se nachází na místě jídla
        
        # Zkontrolujeme počet jídla před odstraněním
        self.assertEqual(len(self.food_factory.food_list), 1, "Jídlo by mělo být před odstraněním přítomné")
        
        # Odstranění jídla, které had snědl
        self.food_factory.remove_food(food)
        
        self.assertEqual(len(self.food_factory.food_list), 0, "Jídlo by mělo být odstraněno po konzumaci hadem")

    def test_snake_movement(self) -> None:
        """
        Testuje, zda se had správně pohybuje v nastaveném směru.

        Scénář:
            1. Nastavíme počáteční pozici hada a směr pohybu.
            2. Posuneme hada a ověříme novou pozici.

        Očekávaný výsledek:
            Had by se měl posunout o jednu jednotku ve směru, který byl nastaven.
        """

        # Nastavit pozici hada a směr
        self.snake.positions = [(GRID_WIDTH // 2, GRID_HEIGHT // 2)]
        self.snake.change_direction((1, 0))  # Směr doprava

        # Simulace pohybu hada
        self.snake.move()
        
        # Očekávaná pozice po pohybu
        expected_position = (GRID_WIDTH // 2 + 1, GRID_HEIGHT // 2)
        self.assertEqual(self.snake.positions[0], expected_position, "Pohyb hada není správný")

if __name__ == '__main__':
    unittest.main()

