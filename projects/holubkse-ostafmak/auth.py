import jwt
import time
import bcrypt
import streamlit as st
import mysql.connector

SECRET_KEY = "JWT_TOKEN_SECRET_PASSWORD"  # Secret key for JWT encoding and decoding

def connect_db():
    """
    Establishes a connection to the MySQL database.

    Connects to the MySQL database using the provided host, user, database name,password, and port.
    Returns the connection object if successful.

    """
    try:
        connection = mysql.connector.connect(
            host="sql7.freesqldatabase.com",  # Host address of the database
            user="sql7728018",                # Username for the database
            database="sql7728018",            # Database name
            password="uNm7MaibUG",            # Password for the database
            port="3306"                       # Port number for the connection
        )
        return connection
    except mysql.connector.Error as e:
        # Display error message if connection fails
        st.error(f"Error connecting to the database: {e}")



def jwt_create_token(username):
    """
    Creates a JWT token for the specified user.
    Generates a JSON Web Token (JWT) for the given username with a 1-hour expiration time.

    """
    # Create a JWT token with a 1-hour expiration time
    payload = {
        "username": username,
        "exp": time.time() + 3600  # term of token work (1 hour)
    }
    token = jwt.encode(payload, SECRET_KEY, algorithm="HS256")
    return token

def jwt_verify_token(token):
    """
    Decodes and verifies a JWT token.

    Decodes the given JSON Web Token (JWT) and verifies its validity using the secret key. 
    If the token is valid, returns the username contained within the token.

    """
    # Decode and verify the JWT token
    try:
        decoded_token = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
        return decoded_token["username"]
    except jwt.ExpiredSignatureError:
        st.error("Session expired. Please log in again.")
    except jwt.InvalidTokenError:
        st.error("Invalid token. Please log in again.")
    return None



def add_user(username, password):
    """
    Adds a new user to the database with a securely hashed password.

    Connects to the database, hashes the provided password, and attempts to insert 
    the new user into the 'users' table. If the username already exists, displays an 
    error message and returns False.

    """
    # Connect to the database
    conn = connect_db() 
    if not conn:
        return False
    
    password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt(12))

    try:
        with conn.cursor() as c:
            # Insert the new user into the "users" table
            c.execute("INSERT INTO users (username, password_hash) VALUES (%s, %s)", (username, password_hash))
            conn.commit()
            st.success("Your registration was successful. Redirecting to the home page.")
            return True
    except mysql.connector.IntegrityError:
        # Show an error if the username already exists
        st.error("A user with this name already exists.")
        return False
    finally:
        # Close the database connection
        conn.close()


def check_user_exist(username, password):
    """
    Checks if a user exists in the database and verifies the provided password.

    Connects to the database and retrieves the hashed password for the given username.
    If the user exists, it compares the provided password with the stored hashed password.
    Returns True if the password matches, otherwise returns False.

    """
    # Connect to the database
    conn = connect_db()
    if not conn:
        return False
    
    try: 
        with conn.cursor() as c:
            # Query the database for the user's hashed password
            c.execute("SELECT password_hash FROM users WHERE username = %s", (username,))
            user = c.fetchone()
    finally:
        # Close the database connection
        conn.close()

    if user:  # If the user exists
        password_hash = user[0]
        # Check if the provided password matches the stored hashed password
        if bcrypt.checkpw(password.encode('utf-8'), password_hash.encode('utf-8')):
            return True
    return False  # Return False if the user does not exist or if the password is incorrect


 
def login():
    """
    Displays a login form and handles user authentication.

    Verifies the provided username and password. If valid, generates and returns a JWT token.
    Displays an error message if the credentials are invalid or any fields are empty.

    """
    st.title("Log in")
    username = st.text_input("Username", key="exist_username")
    password = st.text_input("Password", type="password", key="exist_password")

    if st.button("Log in", key="login_button"):
        # Check if the username and password fields are not empty
        if username.strip() and password.strip():
            if check_user_exist(username, password):  # Temporary hardcoded check for testing

                # Create JWT token for authenticated user
                token = jwt_create_token(username)
                return token  # Return JWT token instead of username
            else:
                # Show error if username or password is invalid
                st.error("Invalid username or password.")
        else:
            # Show error if any field is empty
            st.error("Fields cannot be empty. Please enter your username and password.")

def signin():
    """
    Handles user registration and returns a JWT token if successful.

    Displays a sign-up form, checks for valid input, and registers a new user.
    Returns a JWT token if registration is successful or shows an error message if not.

    """
    st.title("Sign In")
    username = st.text_input("Username", key="reg_username")
    password = st.text_input("Password", type="password", key="reg_password")

    if st.button("Sign In", key="signin_button"):
        # Check if the username and password fields are not empty
        if username.strip() and password.strip():
            # Attempt to add a new user to the database
            if add_user(username, password):
                # Create JWT token for the newly registered user
                token = jwt_create_token(username)
                return token  # Return JWT token instead of username
        else:
            # Show error if any field is empty
            st.error("Fields cannot be empty. Please enter a username and password.")

def logout():
    """
    Logs out the user by clearing the session state.

    Resets the session state to log the user out and displays a success message.
    Pauses for 2 seconds to allow the user to see the logout confirmation.
    
    """
    # Clear the session state to log out the user
    st.session_state["logged_in"] = False
    st.session_state["username"] = ''
    st.success("You have logged out successfully.")
    time.sleep(2)  # Wait for 2 seconds to allow the user to see the message
