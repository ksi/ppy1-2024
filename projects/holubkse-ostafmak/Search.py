import streamlit as st
import pandas as pd

def run():
    """
    Displays an interactive interface for filtering and displaying game data.

    Loads game data from a CSV file and provides filtering options by genres and developers.
    Filters the data based on the user's selection and displays the filtered results, 
    including images, titles, developers, and genres of the games.

    Uses:
        - Streamlit
        - Pandas

    """
    # Load data from CSV file
    @st.cache_data
    def load_data():
        data = pd.read_csv('data.csv')
        return data

    data = load_data()

    # Filter options for genres
    genres = data['Genres'].str.split(', ').explode().unique()
    selected_genres = st.multiselect('Choose genres', genres)

    # Filter options for developers
    developers = data['Developer'].unique()
    selected_developers = st.multiselect('Choose Developers', developers)

    # Apply filters based on user selection
    filtered_data = data.copy()

    if selected_genres:
        filtered_data = filtered_data[filtered_data['Genres'].apply(lambda x: all(genre in x for genre in selected_genres))]
        
    if selected_developers:
        filtered_data = filtered_data[filtered_data['Developer'].isin(selected_developers)]

    # Pagination settings
    page_size = 10
    total_pages = (len(filtered_data) // page_size) + 1
    current_page = st.number_input('Page number', min_value=1, max_value=total_pages, value=1)

    # Paginate the filtered data
    start_idx = (current_page - 1) * page_size
    end_idx = start_idx + page_size
    paginated_data = filtered_data.iloc[start_idx:end_idx]

    # Showing filtered data
    st.write('Filtered data:')
    for _, row in paginated_data.iterrows():
        st.image(row['Pictures'], caption=row['Title'], width=620)
        st.write(f"**Name of game:** {row['Title']}")
        st.write(f"**Developer:** {row['Developer']}")
        st.write(f"**Genres:** {row['Genres']}")
        st.write("---")  # Separator between games
