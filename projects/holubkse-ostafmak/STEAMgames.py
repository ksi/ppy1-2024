import csv
import requests
from bs4 import BeautifulSoup
import streamlit as st

def get_data() -> list[tuple[str, list[str], list[str], str]]:
    """
    Parsing game data from a list of URLs.

    Parses game information such as title, developers, genres, and image URLs 
    from web pages listed in a CSV file.
    
    """
    # Read links from the CSV file
    with open('games_links.csv', 'r') as file:
        links = file.read().splitlines()

    info = []

    # Request data from each link
    for link in links:
        if not link.startswith("http://") and not link.startswith("https://"):
            continue

        try:
            r = requests.get(link)
            soup = BeautifulSoup(r.text, 'lxml')

            # Initialize variables to store parsed data: authors (developers),
            authors = []
            genres = []
            pic_url = ""

            # Parse game title
            title_container = soup.find('div', class_="apphub_AppName")
            title = title_container.text.strip() if title_container else "Unknown Title"

            # Parse developers
            author_container = soup.find('div', id="developers_list")
            if author_container:
                author_elements = author_container.find_all('a', href=True)
                authors = [author.text.strip() for author in author_elements]

            # Parse genres
            genres_container = soup.find('span', {'data-panel': '{"flow-children":"row"}'})
            if genres_container:
                genre_elements = genres_container.find_all('a', href=True)
                genres = [genre.text.strip() for genre in genre_elements]

            # Parse image URL
            pic_box = soup.find('img', class_="game_header_image_full")
            if pic_box and 'src' in pic_box.attrs:
                pic_url = pic_box['src']
            
            # Add parsed data to list if everything is correct 
            if title and authors and genres:
                info.append((title, authors, genres, pic_url))

        except requests.exceptions.RequestException as e:
            st.error(f"Error getting acsses to:  {link}: {e}")
    return info

def save_links_csv(info: list[tuple[str, list[str], list[str], str]], file_name: str) -> None:
    """
    Saves game data to a CSV file.

    Writes the game information, including title, developers, genres, and image URLs, 
    to a CSV file with specified column headers.

    """
    with open(file_name, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        # Write the column headers
        writer.writerow(['Title', 'Developer', 'Genres', 'Pictures'])
        # Write each game's data to the CSV file
        for title, authors, genres, pic_url in info:
            writer.writerow([title, ', '.join(authors), ', '.join(genres), pic_url])

games = get_data() # Get game data from the web 
save_links_csv(games, 'data.csv') # Save game data to CSV file

'''
for title, authors, genres, pic_url in games: # Show game data with images
    if pic_url:
        st.image(pic_url, caption=title, use_column_width=True)
    else:
        st.write("Image dont found")

    st.write(f"Game: {title}")
    st.write(f"Developer: {', '.join(authors)}")
    st.write(f"Genres: {', '.join(genres)}")
    st.write(" ")
'''
