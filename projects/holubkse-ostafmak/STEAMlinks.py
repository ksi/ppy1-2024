import csv
from selenium import webdriver 
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def games_links(url: str, class_name: str, max_links: int) -> list[str]:
    """
    Retrieves a list of unique game links from a webpage.

    Opens the specified URL in a headless Chrome browser, finds game links 
    using the provided class name, and collects up to the specified maximum number of links.
    
    """
    # Set up Chrome options for headless browsing
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--window-size=1920,1080")

    # Open the webpage with Chrome
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(url)

    

    links: list[str] = [] # Store game links
    try:     
        while len(links) < max_links:
            try:
                # Find all game links on the page
                games_elements = WebDriverWait(driver, 20).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, f'a:has(.{class_name.replace(" ", ".")})')))
                # Get href attributes and filter out duplicates
                new_links = [link.get_attribute('href') for link in games_elements if link.get_attribute('href')]
                new_unique_links = [link for link in new_links if link not in links]
                links.extend(new_unique_links)
                
                # Stop if enough links are collected
                if len(links) >= max_links: 
                    break
            
            except Exception as e:
                print(f"mistake during getting links 1 : {type(e).__name__}: {e}")
            
            if len(links) < max_links:
                try:
                    # Click the "Load More" button to get more links
                    wait = WebDriverWait(driver, 10)  # waiting for 10 seconds
                    button = wait.until(EC.element_to_be_clickable((By.XPATH, '//button[contains(@class, "_2tkiJ4VfEdI9kq1agjZyNz")]')))
                    button.click()  
                    print('Button pressed sucsseccfully ')
                except Exception as e:
                    print(f'Can`t press button, reason: {e}')
                    break
    finally:
        driver.quit() # Close the browser
    return links



def save_links_csv(links: list[str], file_name: str) -> None:
    """
    Saves a list of game links to a CSV file.

    """
    with open(file_name, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(['Game links'])
        for link in links:
            writer.writerow([link])

# Main script to start the parsing process
if __name__ == "__main__":
    url = 'https://store.steampowered.com/games?flavor=contenthub_all'
    class_name = '_2ekpT6PjwtcFaT4jLQehUK StoreSaleWidgetTitle'   # Class name for game titles
    max_links = 2000

    # Get game links from the webpage
    links_list = games_links(url, class_name, max_links)
    
    save_links_csv(links_list, 'games_links.csv') # Save links to a CSV file
    print(links_list) # Output the links
