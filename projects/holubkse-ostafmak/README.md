Project "NEXT" Documentation

Overview
This project is designed to scrape game data from online sources and display the information,
including the game's title, developers, genres, and associated images. It consists of two main parts:
one that scrapes game links from a website using Selenium, and another that retrieves detailed game information from the collected links using BeautifulSoup. 
The results are then displayed on a Streamlit-based web interface and saved to a CSV file.

1. Project Structure
Files:
    - app.py: File which is responsible for initialization of authorisation managers (such as cookies, JWT tokens) and also for displaying navigation of web application.
    - auth.py: Additional utilities file for managing user authentication (which provides possibility of logging in and signing in).
    - Search.py: Handles uploading files (file which provides possibility of browzing and filtering games subsequently, which provisez rapid loading of games information).
    - data.csv, games_links.csv: Files which containes games data (titles, authors*, genres, images).  
    - STEAMgames.py: File which provides data parsing from Steam webstore.
    - STEAMlinks.py: Vault of data (game links from steam).

    *authors == developers

2. Functionality
Link Scraper (games_links function in app.py)
This function is responsible for gathering game links from the Steam webstore.

URL: The Steam content hub (https://store.steampowered.com/games?flavor=contenthub_all).
Class Selector: Looks for elements with the class _2ekpT6PjwtcFaT4jLQehUK StoreSaleWidgetTitle.
Max Links: Limits the number of links scraped to max_links (up to 2000).

Key Steps:
A headless Chrome browser is launched using Selenium.
The page is loaded, and the scraper waits for all elements with the game title class to appear.
Unique links are collected until the max_links limit is reached.
If the page supports scrolling or button interaction, it clicks the button to load more items.
The links are saved to a games_links.csv file using save_links_csv.

Function Signatures:
def games_links(url: str, class_name: str, max_links: int) -> list[str]
def save_links_csv(links: list[str], file_name: str) -> None
Game Data Scraper (get_data function in app.py)
Once the links are collected, this function parses the game details (title, developers, genres, image URL) using BeautifulSoup.

Input: List of links from games_links.csv.
Output: A list of tuples with the game title, developers, genres, and image URLs.
Key Steps: Each link is accessed, and the HTML is parsed.

Game information is extracted from the relevant HTML elements:
    - Title: Found within the apphub_AppName div.
    - Developers: Found in the developers_list div.
    - Genres: Extracted from the span element with data-panel attribute.
    - Image URL: Retrieved from an img tag with the game_header_image_full class.

The data is appended to a list if all fields are valid (title, authors, genres, image URL).
Data Saving in .csv format and Display on Streamlit.

The scraped data is displayed on the Streamlit interface. Each game’s image (if available) is shown along with the title, developers, and genres.
If no image is found, a default message is displayed.

Example:
(Python 3.12.6)

for title, authors, genres, pic_url in games:
    if pic_url:
        st.image(pic_url, caption=title, use_column_width=True)
    else:
        st.write("Image not found")
    st.write(f"Game: {title}")
    st.write(f"Developer: {', '.join(authors)}")
    st.write(f"Genres: {', '.join(genres)}")

Saving Data to CSV:
The parsed game data is saved in a CSV file named data.csv with columns: Title, Developer, Genres, and Pictures.

3. Requirements
Libraries: 
    - requests: For making HTTP requests.
    - BeautifulSoup: For parsing HTML.
    - selenium: For web scraping and interacting with dynamic pages.
    - csv: For reading and writing CSV files.
    - pandas: For reading CSV data.
    - streamlit: For displaying a web interface.
    - streamlit_option_menu: To manage the Streamlit sidebar menu.
    - streamlit_cookies_manager: To manage cookies in Streamlit.
    - jwt: To manage JWT tokens.
    - mysql.connector: For connecting to a MySQL database.
    - bcrypt: For password hashing and verification.

Dependencies:
Install the required packages using pip install -r requirements.txt (pip install requests beautifulsoup4 streamlit selenium).

4. Usage

Scraping Game Links:
    Running scraper:
        bash:
            python run STEAMlinks.py

Scraping Game Data:
    Running scraper:
        bash:
            python run STEAMgames.py

After running the scraper and gathering the links, launch the Streamlit interface to view the data:
Running the Project:
    bash:
        python -m streamlit run app.py      or      streamlit run app.py

CSV Output:
The data will be saved into data.csv after the scraping is complete.
Handles exceptions when attempting to click the "Load More" button on the Steam website.

5. Future Improvements
Add more detailed error logging and retry mechanisms.
Optimize the parser to handle larger datasets or implement pagination.
Expand the parser to include additional game metadata, such as release date, reviews, and platform compatibility.
Adjust a "lazy downloading" on the Search page.
