import streamlit as st
from streamlit_option_menu import option_menu
from streamlit_cookies_manager import EncryptedCookieManager
from auth import login, signin, logout, jwt_verify_token

# Establish cookies for the session with encryption
cookies = EncryptedCookieManager(
    prefix="NEXT",
    password='COOKIES_SECRET_PASSWORD'
)

# Check if the cookie manager is ready
if not cookies.ready():
    st.error('Cookies manager not ready. Please refresh the page.')
if 'logged_in' not in st.session_state:
    st.session_state['logged_in'] = False
if 'username' not in st.session_state:
    st.session_state['username'] = ''

# Handle JWT cookies for user authentication
if cookies.ready():
    # Retrieve authentication token from cookies
    token_from_cookies = cookies.get('auth_token')
    
    if token_from_cookies:  # If token exists
        # Verify token and update session state if valid
        username_from_token = jwt_verify_token(token_from_cookies)
        
        if username_from_token:
            st.session_state['logged_in'] = True
            st.session_state['username'] = username_from_token

def navigation():
    """
    Manages the navigation of the application based on the user's authentication status.

    If the user is NOT authenticated, it shows options to "Log In" or "Sign In".
    If the user is authenticated, it shows options to navigate to other pages, including "Log Out".

    Functionality:
    - "Sign In": Registers the user in the database, stores the JWT token in cookies, and reloads the page.
    - "Log In": Authenticates the user, stores the JWT token in cookies, and reloads the page.
    - "Home": Displays the home page.
    - "Search": Loads the module for searching parsed content (game data) by filters.
    - "Log Out": Logs the user out, clears the JWT token from cookies, and reloads the page.
    """
    # If the user is not authenticated
    if not st.session_state['logged_in']:
        with st.sidebar:  # Sidebar navigation for authentication
            selected = option_menu(
                menu_title=None,
                options=['Log In', 'Sign In'],
                icons=['people-fill', 'person-plus-fill'],
                default_index=0,
                key='auth_menu'
            )

        # Log in functionality
        if selected == "Log In":
            token = login()
            if token:
                st.session_state['logged_in'] = True
                cookies['auth_token'] = token  # Save JWT token in cookies
                cookies.save()
                st.rerun()

        # Sign in functionality
        elif selected == "Sign In":
            token = signin()
            if token:
                st.session_state['logged_in'] = True
                cookies['auth_token'] = token  # Save JWT token in cookies
                cookies.save()
                st.success("Registration successful. Please log in.")
                st.rerun()

    # If the user is authenticated
    if st.session_state['logged_in']:
        username = st.session_state.get('username', '')
        with st.sidebar:  # Sidebar navigation for authenticated users
            selected = option_menu(
                menu_title=f'Hello, {username}',
                menu_icon='layout-sidebar',
                options=['Home', 'Search', 'Log Out'],
                icons=['house-door', 'controller', 'box-arrow-left'],
                default_index=0,
                key='logged_in_menu'
            )

        # Logic for page navigation based on user selection
        if selected == "Home":
            st.title(f"Welcome to the Home page, {username}.")
            st.write("With this web application, you can find games you might like by simply selecting a few filters. Once you've played and want to find next game, come back to NEXT!")
            st.write("Currently, you may not be able to use all the features, but the project is still under development, so don’t be discouraged!")
            return NotImplementedError

        elif selected == "Search":
            st.write("File Uploader Selected")
            import Search
            Search.run()

        # Log out functionality
        elif selected == "Log Out":
            logout()
            cookies['auth_token'] = ''  # Clear JWT token from cookies
            cookies.save()
            st.rerun()

navigation()
