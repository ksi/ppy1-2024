import streamlit as st
import pandas as pd
from preprocessing import load_data, clean_data
from visual import (
    plot_goals, plot_comparison, plot_performance_over_time, 
    plot_shot_accuracy, plot_correlation_heatmap, plot_radar_chart, 
    plot_performance_dashboard, plot_seasonal_analysis, plot_moving_average
)

def main() -> None:
    """
    Main function to run the Streamlit football player performance analysis app.
    It loads, cleans, and visualizes player data using various plots.
    """
    # Load and clean data
    df: pd.DataFrame | None = load_data('data.csv')
    
    if df is None:
        st.error("Failed to load data.")
        return
    
    df = clean_data(df)
    
    if df is None:
        st.error("Failed to clean data.")
        return
    
    st.title('Football Player Performance Analysis')

    # Check if data has the expected columns
    if 'Player Names' not in df.columns:
        st.error("Data does not contain 'Player Names' column.")
        return
    
    player_names: pd.Series = df['Player Names'].unique()
    selected_player: str = st.selectbox('Select a Player:', player_names)

    if selected_player:
        st.subheader(f'{selected_player} Performance')

        # Plot goals
        fig = plot_goals(df, selected_player)
        st.plotly_chart(fig)

        # Plot performance over time
        fig_performance = plot_performance_over_time(df, selected_player)
        st.plotly_chart(fig_performance)

        # Plot shot accuracy
        fig_shot_accuracy = plot_shot_accuracy(df, selected_player)
        st.plotly_chart(fig_shot_accuracy)

        # Moving Average Plot
        selected_metric: str = st.selectbox('Select Metric for Moving Average:', ['Goals', 'xG', 'Shots', 'OnTarget'])
        if selected_metric:
            plot_moving_average(df, selected_player, selected_metric)

    # Compare multiple players
    selected_players: list[str] = st.multiselect('Select Players for Comparison:', player_names)
    if len(selected_players) < 2:
        st.warning("Select at least two players for comparison.")
    else:
        fig_comparison = plot_comparison(df, selected_players)
        st.plotly_chart(fig_comparison)

    # Plot Correlation Heatmap
    if st.checkbox('Show Correlation Heatmap'):
        plot_correlation_heatmap(df)

    # Radar Chart Comparison
    if st.checkbox('Show Radar Chart Comparison'):
        if len(selected_players) < 2:
            st.warning("Select at least two players for radar chart comparison.")
        else:
            plot_radar_chart(df, selected_players)

    # Performance Dashboard
    if st.checkbox('Show Performance Dashboard'):
        plot_performance_dashboard(df)

    # Seasonal Analysis
    if st.checkbox('Show Seasonal Analysis'):
        plot_seasonal_analysis(df)

if __name__ == "__main__":
    main()







