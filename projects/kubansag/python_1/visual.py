import plotly.graph_objects as go
import plotly.express as px
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import streamlit as st

def plot_goals(df: pd.DataFrame, player_name: str) -> go.Figure:
    """
    Plot a player's total goals for either a single year (pie chart) or multiple years (bar chart).

    Args:
        df (pd.DataFrame): The player performance data.
        player_name (str): The name of the player to visualize.

    Returns:
        go.Figure: A Plotly figure showing the goals.
    """
    player_data = df[df['Player Names'] == player_name]
    player_data = player_data.drop_duplicates(subset=['Year'])
    fig = go.Figure()

    if player_data['Year'].nunique() == 1:
        fig.add_trace(go.Pie(
            labels=['Goals'],
            values=[player_data['Goals'].values[0]],
            name='Goals',
            marker_colors=['royalblue']
        ))
        fig.update_layout(
            title=f'{player_name} Goals for the Year {player_data["Year"].values[0]}',
        )
    else:
        fig.add_trace(go.Bar(
            x=player_data['Year'],
            y=player_data['Goals'],
            name='Goals',
            marker_color='royalblue'
        ))
        fig.update_layout(
            title=f'{player_name} Goals by Year',
            xaxis_title='Year',
            yaxis_title='Goals'
        )
    
    return fig

def plot_comparison(df: pd.DataFrame, players: list[str]) -> go.Figure:
    """
    Compare the goals scored by multiple players using a bar chart.

    Args:
        df (pd.DataFrame): The player performance data.
        players (list[str]): A list of player names to compare.

    Returns:
        go.Figure: A Plotly figure showing the comparison.
    """
    comparison_data = df[df['Player Names'].isin(players)]
    fig = px.bar(comparison_data, x='Player Names', y='Goals', color='Player Names', barmode='group', title='Goals Comparison')
    return fig

def plot_performance_over_time(df: pd.DataFrame, player_name: str) -> go.Figure:
    """
    Plot a player's performance over time, including goals and expected goals (xG).

    Args:
        df (pd.DataFrame): The player performance data.
        player_name (str): The name of the player to visualize.

    Returns:
        go.Figure: A Plotly figure showing the player's performance over time.
    """
    player_data = df[df['Player Names'] == player_name]
    fig = go.Figure()
    
    fig.add_trace(go.Scatter(
        x=player_data['Year'],
        y=player_data['Goals'],
        mode='lines+markers',
        name='Goals'
    ))
    
    fig.add_trace(go.Scatter(
        x=player_data['Year'],
        y=player_data['xG'],
        mode='lines+markers',
        name='xG'
    ))
    
    fig.update_layout(
        title=f'{player_name} Performance Over Time',
        xaxis_title='Year',
        yaxis_title='Value'
    )
    
    return fig

def plot_shot_accuracy(df: pd.DataFrame, player_name: str) -> go.Figure:
    """
    Plot a player's shot accuracy over time, comparing total shots and shots on target.

    Args:
        df (pd.DataFrame): The player performance data.
        player_name (str): The name of the player to visualize.

    Returns:
        go.Figure: A Plotly figure showing the player's shot accuracy over time.
    """
    player_data = df[df['Player Names'] == player_name]
    fig = go.Figure()
    
    fig.add_trace(go.Bar(
        x=player_data['Year'],
        y=player_data['Shots'],
        name='Total Shots'
    ))
    
    fig.add_trace(go.Bar(
        x=player_data['Year'],
        y=player_data['OnTarget'],
        name='Shots on Target'
    ))
    
    fig.update_layout(
        title=f'{player_name} Shot Accuracy by Year',
        xaxis_title='Year',
        yaxis_title='Shots',
        barmode='group'
    )
    
    return fig

def plot_correlation_heatmap(df: pd.DataFrame) -> None:
    """
    Plot a correlation heatmap of selected performance metrics.

    Args:
        df (pd.DataFrame): The player performance data.
    """
    correlation = df[['Goals', 'xG', 'Shots', 'OnTarget']].corr()
    fig, ax = plt.subplots()
    sns.heatmap(correlation, annot=True, cmap='coolwarm', ax=ax)
    st.pyplot(fig)

def plot_radar_chart(df: pd.DataFrame, players: list[str]) -> None:
    """
    Plot a radar chart to compare multiple players based on key performance metrics.

    Args:
        df (pd.DataFrame): The player performance data.
        players (list[str]): A list of player names to compare.
    """
    metrics: list[str] = ['Goals', 'xG', 'Shots', 'OnTarget']
    player_data = df[df['Player Names'].isin(players)]
    
    fig = go.Figure()
    
    for player in players:
        player_stats = player_data[player_data['Player Names'] == player][metrics].mean().values
        fig.add_trace(go.Scatterpolar(
            r=player_stats,
            theta=metrics,
            fill='toself',
            name=player
        ))
    
    fig.update_layout(
        polar=dict(
            radialaxis=dict(visible=True)
        ),
        title='Radar Chart Comparison'
    )
    
    st.plotly_chart(fig)

def plot_performance_dashboard(df: pd.DataFrame) -> None:
    """
    Display a dashboard of player performance metrics over time.

    Args:
        df (pd.DataFrame): The player performance data.
    """
    metrics: list[str] = st.multiselect('Select Metrics to Display:', ['Goals', 'xG', 'Shots', 'OnTarget'])
    
    if metrics:
        for metric in metrics:
            fig = go.Figure()
            for player in df['Player Names'].unique():
                player_data = df[df['Player Names'] == player]
                fig.add_trace(go.Scatter(x=player_data['Year'], y=player_data[metric], mode='lines+markers', name=player))
            
            fig.update_layout(title=f'{metric} Performance Across Players', xaxis_title='Year', yaxis_title=metric)
            st.plotly_chart(fig)

def plot_seasonal_analysis(df: pd.DataFrame) -> None:
    """
    Perform a seasonal analysis of player performance metrics.

    Args:
        df (pd.DataFrame): The player performance data.
    """
    seasons: list[int] = df['Year'].unique()
    selected_season: int = st.selectbox('Select a Season:', seasons)
    
    season_data = df[df['Year'] == selected_season]
    
    fig = go.Figure()
    for metric in ['Goals', 'xG', 'Shots', 'OnTarget']:
        fig.add_trace(go.Bar(x=season_data['Player Names'], y=season_data[metric], name=metric))
    
    fig.update_layout(title=f'Performance Metrics for Season {selected_season}', xaxis_title='Player Names', yaxis_title='Value')
    st.plotly_chart(fig)

def plot_moving_average(df: pd.DataFrame, player_name: str, metric: str, window_size: int = 5) -> None:
    """
    Plot a player's performance metric with a moving average.

    Args:
        df (pd.DataFrame): The player performance data.
        player_name (str): The name of the player to visualize.
        metric (str): The metric to plot.
        window_size (int): The window size for the moving average. Default is 5.
    """
    player_data = df[df['Player Names'] == player_name]
    player_data = player_data.sort_values(by='Year')

    player_data[f'{metric}_MA'] = player_data[metric].rolling(window=window_size).mean()

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=player_data['Year'], y=player_data[metric], mode='lines+markers', name=f'{metric}'))
    fig.add_trace(go.Scatter(x=player_data['Year'], y=player_data[f'{metric}_MA'], mode='lines', name=f'{metric} Moving Average'))
    
    fig.update_layout(title=f'{player_name} {metric} with Moving Average', xaxis_title='Year', yaxis_title=metric)
    st.plotly_chart(fig)




