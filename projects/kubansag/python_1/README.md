# Football Performance Analysis

This project provides an interactive web application for analyzing and visualizing football player performance data from World Leagues. It allows users to explore various metrics, compare players, and gain insights into player performance trends over time.

## Features

- **Player Performance Visualization**: View goals, xG, and shot accuracy for individual players.
- **Comparative Analysis**: Compare performance metrics of multiple players.
- **Performance Over Time**: Analyze how a player's performance evolves over different years.
- **Correlation Heatmap**: Visualize correlations between different performance metrics.
- **Radar Chart Comparison**: Compare multiple players across various metrics using radar charts.
- **Performance Dashboard**: Interactive dashboard to display selected performance metrics across players.
- **Seasonal Analysis**: View performance metrics for selected seasons.
- **Moving Average Trend**: Analyze moving averages for selected performance metrics.

## Requirements

- Python 3.x
- Streamlit
- Plotly
- Seaborn
- Pandas
- Matplotlib

## Usage

1. Prepared data in a CSV file named `data.csv` with the following columns:
    - Country
    - League
    - Club
    - Player Names
    - Matches_Played
    - Substitution
    - Mins
    - Goals
    - xG
    - xG Per Avg Match
    - Shots
    - OnTarget
    - Shots Per Avg Match
    - On Target Per Avg Match
    - Year
2. Run the Streamlit application:
    ```bash
    streamlit run app.py
    ```
3. Open your web browser and navigate to the URL provided by Streamlit to interact with the application.

## File Structure

- `data_processing.py`: Contains functions for loading and cleaning the data.
- `visual.py`: Includes plotting functions for various visualizations.
- `app.py`: Main Streamlit app integrating data processing and visualizations.
