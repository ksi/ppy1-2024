import pandas as pd

def load_data(filepath: str) -> pd.DataFrame | None:
    """
    Load football player performance data from a CSV file.

    Args:
        filepath (str): The file path to the CSV file.

    Returns:
        pd.DataFrame | None: A DataFrame containing the loaded data or None if an error occurs.
    """
    try:
        df: pd.DataFrame = pd.read_csv(filepath)
        print(df.head())  # Debugging: Print the first few rows of the dataframe
        return df
    except Exception as e:
        print(f"Error loading data: {e}")
        return None

def clean_data(df: pd.DataFrame) -> pd.DataFrame | None:
    """
    Clean the football player performance dataset by stripping column names, 
    dropping missing values, and converting specific columns to numeric types.

    Args:
        df (pd.DataFrame): The input DataFrame containing player data.

    Returns:
        pd.DataFrame | None: The cleaned DataFrame or None if an error occurs.
    """
    if df is None:
        return None
    
    try:
        # Strip whitespace from column names
        df.columns = df.columns.str.strip()
        
        # Drop rows with any missing values
        df = df.dropna()
        
        # Convert numeric columns to appropriate types
        numeric_cols: list[str] = ['Matches_Played', 'Substitution', 'Mins', 'Goals', 'xG', 'Shots', 'OnTarget']
        for col in numeric_cols:
            df[col] = pd.to_numeric(df[col], errors='coerce')
        
        # Drop rows that couldn't be converted to numeric (if any)
        df = df.dropna()
        
        return df
    except Exception as e:
        print(f"Error cleaning data: {e}")
        return None


    


