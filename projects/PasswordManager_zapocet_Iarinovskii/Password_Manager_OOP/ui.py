from password_generator import generate_password
import json
from tkinter import *
from tkinter import messagebox


class Interface:
    def __init__(self):
        # ---------------------------- UI SETUP ------------------------------- #
        self.window = Tk()
        self.messagebox = messagebox
        self.window.config(padx=50, pady=50)
        self.window.title("Password Manager")
        self.window.minsize(height=200, width=200)

        self.canvas = Canvas(height=200, width=200)
        logo_img = PhotoImage(file="logo.png", )
        self.canvas.create_image(100, 100, image=logo_img)
        self.canvas.grid(column=1, row=0)

        self.website_label = Label(text="Website:")
        self.website_label.grid(column=0, row=1)
        # ------------------------------------------------
        self.website_entry = Entry(width=44)
        self.website_entry.focus()
        self.website_entry.grid(column=1, row=1, columnspan=2, sticky="E")

        self.username_label = Label(text="Email/Username:")
        self.username_label.grid(column=0, row=2)
        # ------------------------------------------------
        self.username_entry = Entry(width=44)
        self.username_entry.grid(column=1, row=2, columnspan=2, sticky="E")

        self.password_label = Label(text="Password:")
        self.password_label.grid(column=0, row=3)
        # ------------------------------------------------
        self.password_entry = Entry(width=26)
        self.password_entry.grid(column=1, row=3, sticky="E")
        # ------------------------------------------------
        self.password_button = Button(text="Generate Password", command=self.generate_and_fill)
        self.password_button.grid(column=2, row=3, sticky="W")

        self.add_button = Button(text="Add", width=37, command=self.save)
        self.add_button.grid(column=1, row=4, columnspan=2, sticky="E")

        # ------------------------------------------------

    def run(self) -> None:
        """
        Activates the mainloop of TKinter
        :return: None
        """
        self.canvas = Canvas(height=200, width=200)
        logo_img = PhotoImage(file="logo.png", )
        self.canvas.create_image(100, 100, image=logo_img)
        self.canvas.grid(column=1, row=0)
        self.window.mainloop()

    def generate_and_fill(self) -> None:
        """
        :return: None
        """
        new_password = generate_password()
        self.password_entry.delete(0, 'end')  # Clear the current content
        self.password_entry.insert(0, new_password)  # Insert the new password

    def save(self) -> None:
        """
        Saves the credentials in JSON format
        :return: None
        """
        web = self.website_entry.get()
        passwd = self.password_entry.get()
        usr = self.username_entry.get()
        new_data = {
            web: {"email": usr,
                  "password": passwd,
                  }
        }
        if web.strip() == "" or usr.strip() == "" or passwd.strip() == "":
            self.messagebox.showwarning(title="field left empty", message="Please do not leave any field empty.\n")
        else:
            is_ok = messagebox.askokcancel(title=web,
                                           message=f"The details entered are:\nEmail: {usr}\n"
                                                   f"Password: {passwd}\n "
                                                   f"Is it okay to save?")
            if is_ok:
                try:
                    with open("data.json", "r") as data_file:
                        # Reading old data
                        data = json.load(data_file)

                except (FileNotFoundError, json.decoder.JSONDecodeError):
                    with open("data.json", "w") as data_file:
                        json.dump(new_data, data_file, indent=4)
                else:
                    # Updating old data with new data
                    data.update(new_data)

                    # Saving updated data
                    with open("data.json", "w") as data_file:
                        json.dump(data, data_file, indent=4)

                finally:
                    self.website_entry.delete(0, END)
                    self.username_entry.delete(0, END)
                    self.password_entry.delete(0, END)
