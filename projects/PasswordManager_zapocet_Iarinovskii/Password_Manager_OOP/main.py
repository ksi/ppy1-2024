import ui


def main():
    interface = ui.Interface()
    interface.run()  # Start the Tkinter main loop to launch the GUI


if __name__ == "__main__":
    main()
