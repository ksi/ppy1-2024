import pytest
from ui import Interface


@pytest.fixture
def interface():
    return Interface()


def test_save_with_empty_fields(interface, mocker):
    # Arrange
    interface.website_entry.insert(0, "")
    interface.username_entry.insert(0, "user@example.com")
    interface.password_entry.insert(0, "")

    # Mock the showwarning function
    mock_showwarning = mocker.patch.object(interface.messagebox, "showwarning")

    interface.save()

    # Assert
    mock_showwarning.assert_called_once_with(
        title="field left empty", message="Please do not leave any field empty.\n"
    )